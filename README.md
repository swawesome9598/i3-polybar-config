# SCREENSHOT

![i3](https://gitlab.com/swawesome9598/i3-polybar-config/raw/master/rice4.png)

# i3/polybar config

This is my i3gaps/polybar config feel free to modify,use, or fork this repository.

# Dependencies
These are all the dependencies these include applications that are connected to workspaces or just things you need

# i3 GAPS
https://github.com/Airblader/i3

# FONT AWESOME
http://fontawesome.io/

# POLYBAR
https://github.com/jaagr/polybar

# FEH
https://feh.finalrewind.org/ (or for debian/ubuntu apt-get feh)

# RXVT-UNICODE (URXVT)
https://github.com/exg/rxvt-unicode ( or for debian/ubuntu apt-get rxvt-unicode)

# COMPTON 
https://github.com/chjj/compton ( or for debian/ubuntu apt-get compton )

# TERMINUS
http://terminus-font.sourceforge.net/ (or for debian/ubuntu apt-get xfonts-terminus)

